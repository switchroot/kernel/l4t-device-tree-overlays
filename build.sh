#!/bin/bash
cd "$(dirname "${BASH_SOURCE[0]}")"
mkdir -p switchroot/overlays/
dtc -I dts -O dtb -o switchroot/overlays/tegra210-icosa_emmc-overlay.dtbo emmc_overlay.dts
dtc -I dts -O dtb -o switchroot/overlays/tegra210-UART-B-overlay.dtbo uart_b_debug.dts
dtc -I dts -O dtb -o switchroot/overlays/tegra210-icosa_enable_als-overlay.dtbo enable_als.dts
rm disable_rtc_wake.dts.pre || true
cpp -nostdinc -I include -undef -x assembler-with-cpp  disable_rtc_wake.dts disable_rtc_wake.dts.pre
dtc -I dts -O dtb -o switchroot/overlays/tegra210-icosa_disable_rtc_wake-overlay.dtbo disable_rtc_wake.dts.pre
