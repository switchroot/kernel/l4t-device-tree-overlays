# l4t-device-tree-overlays

nfs.txt can only be used with GNU/Linux distribution currently (for nfs boot refer to uenv_readme.txt)

EMMC and UART-B overlays are common to GNU/Linux and Android.
A script is provided to help you build these two overlays


## Building overlays

```sh
sudo apt install device-tree-compiler
```

```sh
cd l4t-device-tree-overlays
```

```sh
./build.sh
```

```sh
cp -r switchroot/ <SD_CARD_FAT_PARTITION>
```
